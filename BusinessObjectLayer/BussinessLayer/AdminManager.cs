﻿using BusinessAccessLayer.Interfaces.AdminManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Repositories;

namespace BusinessAccessLayer.BussinessLayer
{
   public class AdminManager : IAdminManager
    {

        private UnitOfBusinessLogic unitOfBusinessLogic;
        private UnitOfWork unitOfWork;


        public AdminManager(UnitOfBusinessLogic unitOfBusinessLogic)
        {
            this.unitOfBusinessLogic = unitOfBusinessLogic;
            unitOfWork = unitOfBusinessLogic.unitOfWork;
        }

        public string LoginByUserName(string userName, string password,out Admin admin)
        {
            try
            {
                admin = unitOfWork.AdminCredentialRepository.getAll().Where(d => d.username.ToLower() == userName.ToLower() && d.password == password).First().Admin;
                if (admin==null)
                {
                    return null;
                }
                else
                {
                    return unitOfBusinessLogic.AccessManager.generateNewAccessTokenForAdmin(admin.id);
                }
            }
            catch (Exception)
            {
                admin=null;
                return null;
            }
        }

        public string LoginByEmailId(string email, string password, out Admin admin)
        {
            try
            {
                admin = unitOfWork.AdminCredentialRepository.getAll().Where(d => d.email.ToLower() == email.ToLower() && d.password == password).First().Admin;
                if (admin == null)
                {
                    return null;
                }
                else
                {
                    return unitOfBusinessLogic.AccessManager.generateNewAccessTokenForAdmin(admin.id);
                }
            }
            catch (Exception)
            {
                admin = null;
                return null;
            }
        }

        public void activateAdmin(int adminID)
        {
            if (adminID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Admin findAdmin = unitOfWork.AdminRepository.Find(adminID);
                    if (findAdmin != null)
                    {
                        findAdmin.setActive(true);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Admin is not activated against given ID", e);
                }
            }
        }

        public void createNewAdmin(DataAccessLayer.Entities.Admin admin)
        {
            if (admin != null)
            {
                unitOfWork.AdminRepository.AddNew(admin);
                unitOfWork.Save();
            }
        }

        public void deactivateAdmin(int adminID)
        {
            if (adminID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Admin findAdmin = unitOfWork.AdminRepository.Find(adminID);
                    if (findAdmin != null)
                    {
                        findAdmin.setActive(false);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Admin is not deactivated against given ID", e);
                }
            }
        }

        public void deleteAdmin(int adminID)
        {
            if (adminID != 0)
            {
                try
                {
                    unitOfWork.AdminRepository.Delete(adminID);
                    unitOfWork.Save();
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Admin is not deleted against given ID", e);
                }
            }
        }

        public void updateAdmin(int adminID, DataAccessLayer.Entities.Admin admin)
        {
            if (adminID != 0)
            {

                try
                {
                    DataAccessLayer.Entities.Admin findAdmin = unitOfWork.AdminRepository.Find(adminID);
                    if (findAdmin != null)
                    {                        
                        if (admin.firstName != null)
                        {
                            findAdmin.firstName = admin.firstName;
                        }
                        if (admin.lastName != null)
                        {
                            findAdmin.lastName = admin.lastName;
                        }
                        if (admin.phoneno != null)
                        {
                            findAdmin.phoneno = admin.phoneno;
                        }
                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Admin is not updated against given ID", e);
                }
            }
        }

        public void AssignCredentials(int adminID, AdminCredential credentials)
        {
            credentials.id = adminID;
            unitOfWork.AdminCredentialRepository.AddNew(credentials);
            unitOfWork.Save();
        }

        public void updateAdminCredential(int adminID, AdminCredential adminCredential)
        {

            if (adminID != 0)
            {
                try
                {
                    AdminCredential findAdminCredential = unitOfWork.AdminCredentialRepository.Find(adminID);

                    if (findAdminCredential != null)
                    {
                            
                        if (adminCredential.password != null)
                        {
                            findAdminCredential.password = adminCredential.password;
                        }
                       

                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("AdminCredential not found against given ID", e);
                }
            }
        }
    }
}
