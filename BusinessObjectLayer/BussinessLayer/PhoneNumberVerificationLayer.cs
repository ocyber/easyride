﻿using BusinessAccessLayer.Interfaces.PhoneNumberVerification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Repositories;
using DataAccessLayer.Entities;

namespace BusinessAccessLayer.BussinessLayer
{
    public class PhoneNumberVerificationLayer : IPhoneNumberVerificationLayer
    {
        private static object Lock = new object();
        private UnitOfBusinessLogic unitOfBusinessLogic;
        private UnitOfWork unitOfWork;
        
        public PhoneNumberVerificationLayer(UnitOfBusinessLogic unitOfBusinessLogic)
        {
            this.unitOfBusinessLogic = unitOfBusinessLogic;
            unitOfWork = unitOfBusinessLogic.unitOfWork;
        }

        public int CreateVerificationRequest(string phoneNumber, out int RemainingTime)
        {
            int minuts = 3;

            lock (Lock)
            {
                PhoneNumberVerification PNV;
                try
                {
                    PNV = unitOfWork.PhoneNumberVerificationRepository.FindByPhoneNumber(phoneNumber).First();
                    if (PNV == null)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        if (PNV.expiryDate >= DateTime.Now )
                        {
                            RemainingTime = Convert.ToInt32((PNV.expiryDate - DateTime.Now).TotalSeconds);
                            return PNV.Id;
                        }
                        else
                        {
                            PNV.verificationCode = unitOfBusinessLogic.UniqueCodeGenerator.generatePhoneNumberVerificationCode();
                            PNV.expiryDate = DateTime.Now + TimeSpan.FromMinutes(minuts);
                            RemainingTime = minuts * 60;
                        }
                    }

                }
                catch (Exception)
                {
                    PNV = new PhoneNumberVerification();
                    PNV.phoneNumber = phoneNumber;
                    PNV.verificationCode = unitOfBusinessLogic.UniqueCodeGenerator.generatePhoneNumberVerificationCode();
                    PNV.expiryDate = DateTime.Now + TimeSpan.FromMinutes(minuts);
                    RemainingTime = minuts * 60;
                    unitOfWork.PhoneNumberVerificationRepository.AddNew(PNV);

                }
               
                unitOfWork.Save();
                return PNV.Id;
            }


        }

    }
}
