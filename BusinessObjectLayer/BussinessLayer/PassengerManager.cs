﻿using BusinessAccessLayer.Interfaces.PassengerManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Repositories;

namespace BusinessAccessLayer.BussinessLayer
{
    class PassengerManager : IPassengerManager
    {
        private UnitOfBusinessLogic unitOfBusinessLogic;
        private UnitOfWork unitOfWork;

        public void activatePassenger(int passengerID)
        {
            if (passengerID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Passenger findPassenger;
                    findPassenger = unitOfWork.PassengerRepository.Find(passengerID);

                    if (findPassenger != null)
                    {
                        findPassenger.setActive(true);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("Passenger is Not activated againt given ID", e);
                }
            }

        }

        public PassengerManager(UnitOfBusinessLogic unitOfBusinessLogic)
        {
            this.unitOfBusinessLogic = unitOfBusinessLogic;
            unitOfWork = unitOfBusinessLogic.unitOfWork;
        }

        public void createNewPassenger(DataAccessLayer.Entities.Passenger passenger)
        {
            if (passenger  != null)
            {
                unitOfWork.PassengerRepository.AddNew(passenger);
                unitOfWork.Save();
            }
        }

        public void deactivatePassenger(int passengerID)
        {
            if (passengerID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Passenger findPassenger;
                    findPassenger = unitOfWork.PassengerRepository.Find(passengerID);

                    if (findPassenger != null)
                    {
                        findPassenger.setActive(false);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("Passenger is Not Deactivated againt given ID", e);
                }
            }
        }

        public void deletePassenger(int passengerID)
        {
            if (passengerID != 0)
            {
                try
                {
                    unitOfWork.PassengerRepository.Delete(passengerID);
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("Passenger Is Not deleted Against given ID", e);
                }
            }
        }

        public void updatePassenger(int passengerID, DataAccessLayer.Entities.Passenger passenger)
        {
            if (passengerID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Passenger findPassenger = unitOfWork.PassengerRepository.Find(passengerID);

                    if (findPassenger != null)

                    {
                        
                        if (passenger.firstName != null)
                        {
                            findPassenger.firstName = passenger.firstName;
                        }
                        if (passenger.defaultPaymentMode != null)
                        {
                            findPassenger.defaultPaymentMode = passenger.defaultPaymentMode;
                        }
                        if (passenger.lastName != null)
                        {
                            findPassenger.lastName = passenger.lastName;
                        }
                       
                        if (passenger.phoneNo !=null)
                        {
                            findPassenger.phoneNo = passenger.phoneNo;
                        }
                        
                        
                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("Passenger is not Updated against given ID",e);
                }
            }
        }

        public void updatePassengerCreditCard(int passengerID, PassengerCreditCardDetail passengerCreditCardDetail)
        {
            if (passengerID != 0)
            {
                try
                {
                    PassengerCreditCardDetail findPassengerCreditCardDetail = unitOfWork.PassengerCreditCardDetailRepository.Find(passengerID);

                    if (findPassengerCreditCardDetail != null)
                    {
                        if (passengerCreditCardDetail.cardNo != null)
                        {
                            findPassengerCreditCardDetail.cardNo = passengerCreditCardDetail.cardNo;
                        }
                        if (passengerCreditCardDetail.ccvNo != null)
                        {
                            findPassengerCreditCardDetail.ccvNo = passengerCreditCardDetail.ccvNo;
                        }
                        if (passengerCreditCardDetail.expiryDate != null)
                        {
                            findPassengerCreditCardDetail.expiryDate = passengerCreditCardDetail.expiryDate;
                        }
                        if (passengerCreditCardDetail.holderName != null)
                        {
                            findPassengerCreditCardDetail.holderName = passengerCreditCardDetail.holderName;
                        }
                     
                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("PassengercreditCard is not Updated against given ID", e);
                }
            }
        }

        public void updatePassengerCredential(int passengerID, PassengerCredential passengerCredential)
        {
            if (passengerID != 0)
            {
                try
                {
                    PassengerCredential findPassengerCredential = unitOfWork.PassengerCredentialRepository.Find(passengerID);

                    if (findPassengerCredential != null)
                    {
                        
                        if (passengerCredential.password != null)
                        {
                            findPassengerCredential.password = passengerCredential.password;
                        }
                        if (passengerCredential.username != null)
                        {
                            findPassengerCredential.username = passengerCredential.username;
                        }
                        if (passengerCredential.email != null)
                        {
                            findPassengerCredential.email = passengerCredential.email;
                        }

                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("PassengerCredential not found against given ID", e);
                }
            }
        }
    }
}
