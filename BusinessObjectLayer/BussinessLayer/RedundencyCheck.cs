﻿using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.BussinessLayer
{
    public class RedundencyCheck
    {
        private UnitOfBusinessLogic unitOfBusinessLogic;
        private UnitOfWork unitOfWork;


        public RedundencyCheck(UnitOfBusinessLogic unitOfBusinessLogic)
        {
            this.unitOfBusinessLogic = unitOfBusinessLogic;
            unitOfWork = unitOfBusinessLogic.unitOfWork;
        }

        static object EmailCheckLock = new object();


        

        public bool isEmailExist(int companyID, string email)
        {
            if (companyID != 0)
            {
                try
                {
                    Company findCompany = unitOfWork.CompanyRepository.Find(companyID);

                    if (findCompany.email == email)
                    {
                        return true;
                    }
                    if (findCompany.Drivers.Where(d => d.DriverCredential.email == email).ToList()!=null)
                    {
                        return true;
                    }
                    else if (findCompany.Passengers.Where(p => p.PassengerCredential.email == email).ToList() != null)
                    {
                        return true;
                    }
                    else if (findCompany.Admins.Where(a => a.AdminCredential.email == email).ToList() != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Company Not Found Against given ID",e);
                }
            }
            else
            {
                throw new ArgumentNullException("Email cannot be NULL");
            }
        }

        public bool isEmailExistInPublicDomain(string email)
        {
            if (email != null)
            {
                List<Company> companyList = unitOfWork.CompanyRepository.getAll().Where(m => m.mode == Flags.PUBLIC).ToList();

                foreach (var item in companyList)
                {
                    if (isEmailExist(item.id, email))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                throw new ArgumentNullException("Email cannot be NULL");
            }
            
        }

       

        public bool isPhoneNumberExist(int companyID, string phoneNo)
        {
            if (companyID != 0)
            {
                try
                {
                    Company findCompany = unitOfWork.CompanyRepository.Find(companyID);

                    if (findCompany.phoneNo == phoneNo)
                    {
                        return true;
                    }
                    if (findCompany.Drivers.Where(d => d.phoneNo == phoneNo).ToList() != null)
                    {
                        return true;
                    }
                    else if (findCompany.Passengers.Where(p => p.phoneNo == phoneNo).ToList() != null)
                    {
                        return true;
                    }
                    else if (findCompany.Admins.Where(a => a.phoneno == phoneNo).ToList() != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Company Not Found Against given ID", e);
                }
            }
            else
            {
                throw new ArgumentNullException("PhoneNumber cannot be NULL");
            }
        }

        public bool isPhoneNumberExistInPublicDomain(string phoneNo)
        {
            if (phoneNo != null)
            {
                List<Company> companyList = unitOfWork.CompanyRepository.getAll().Where(m => m.mode == Flags.PUBLIC).ToList();

                foreach (var item in companyList)
                {
                    if (isPhoneNumberExist(item.id, phoneNo))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                throw new ArgumentNullException("PhoneNumber cannot be NULL");
            }

        }

        ///////////////////////////////////////////

        public bool isUserNameExist(int companyID, string userName)
        {
            if (companyID != 0)
            {
                try
                {
                    Company findCompany = unitOfWork.CompanyRepository.Find(companyID);

                    
                    if (findCompany.Drivers.Where(d => d.DriverCredential.username == userName).ToList() != null)
                    {
                        return true;
                    }
                    else if (findCompany.Passengers.Where(p => p.PassengerCredential.username == userName).ToList() != null)
                    {
                        return true;
                    }
                    else if (findCompany.Admins.Where(a => a.AdminCredential.username == userName).ToList() != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Company Not Found Against given ID", e);
                }
            }
            else
            {
                throw new ArgumentNullException("UserName cannot be NULL");
            }
        }

        public bool isUserNameExistInPublicDomain(string userName)
        {
            if (userName != null)
            {
                List<Company> companyList = unitOfWork.CompanyRepository.getAll().Where(m => m.mode == Flags.PUBLIC).ToList();

                foreach (var item in companyList)
                {
                    if (isEmailExist(item.id, userName))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                throw new ArgumentNullException("UserName cannot be NULL");
            }

        }
    }
}
