﻿using BusinessAccessLayer.Interfaces.DriverManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Repositories;

namespace BusinessAccessLayer.BussinessLayer
{
    class DriverManager : IDriverManager
    {

        private UnitOfBusinessLogic unitOfBusinessLogic;
        private UnitOfWork unitOfWork;


        public DriverManager(UnitOfBusinessLogic unitOfBusinessLogic)
        {
            this.unitOfBusinessLogic = unitOfBusinessLogic;
            unitOfWork = unitOfBusinessLogic.unitOfWork;
        }


        public void activateDriver(int driverID)
        {
            if (driverID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Driver findDriver = unitOfWork.DriverRepository.Find(driverID);
                    if (findDriver != null)
                    {
                        findDriver.setActive(true);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Driver is not activated against given ID", e);
                }
            }
        }

        public void createNewDriver(DataAccessLayer.Entities.Driver driver)
        {
            if (driver!= null)
            {
                unitOfWork.DriverRepository.AddNew(driver);
                unitOfWork.Save();
            }
        }

        public void deactivateDriver(int driverID)
        {
            if (driverID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Driver findDriver = unitOfWork.DriverRepository.Find(driverID);
                    if (findDriver != null)
                    {
                        findDriver.setActive(false);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Driver is not deactivated against given ID", e);
                }
            }
        }

        public void deleteDriver(int driverID)
        {
            if (driverID != 0)
            {
                try
                {
                    unitOfWork.DriverRepository.Delete(driverID);
                    unitOfWork.Save();
                }
                catch (Exception e)
                {
                    throw new KeyNotFoundException("Driver is not deleted against given ID", e);
                }
            }
        }

        public void updateDriver(int driverID, DataAccessLayer.Entities.Driver driver)
        {
            if (driverID != 0)
            {
                try
                {
                    DataAccessLayer.Entities.Driver findDriver = unitOfWork.DriverRepository.Find(driverID);
                    if (findDriver != null)
                    {
                        if (driver.availabilityStatus != null)
                        {
                            findDriver.availabilityStatus = driver.availabilityStatus;
                        }
                        if (driver.cnic != null)
                        {
                            findDriver.cnic = driver.cnic;
                        }
 
                        if (driver.DOB != null)
                        {
                            findDriver.DOB = driver.DOB;
                        }
                      
                        if (driver.firstName != null)
                        {
                            findDriver.firstName = driver.firstName;
                        }
                        if (driver.lastName != null)
                        {
                            findDriver.lastName = driver.lastName;
                        }
                        if (driver.phoneNo != null)
                        {
                            findDriver.phoneNo = driver.phoneNo;
                        }                       
                    }
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("Driver is not Updated against given ID",e);
                }
            }
        }

        public void updateDriverCredential(int driverID, DriverCredential driverCredential)
        {
            if (driverID != 0)
            {
                try
                {
                    DriverCredential findDriverCredential = unitOfWork.DriverCredentialRepository.Find(driverID);

                    if (findDriverCredential != null)
                    {
                       
                        if (driverCredential.password != null)
                        {
                            findDriverCredential.password = driverCredential.password;
                        }
                        if (driverCredential.username != null)
                        {
                            findDriverCredential.username = driverCredential.username;
                        }
                        
                    }
                    unitOfWork.Save();
                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("DriverCredential not found against given ID",e);
                }
            }
        }

        

        public void updateDriverCreditCard(int driverID, DriverCreditCardDetail driverCreditCardDetail)
        {
            {            if (driverID != 0)

                try
                {
                    DriverCreditCardDetail findDriverCreditCardDetail;
                    findDriverCreditCardDetail = unitOfWork.DriverCreditCardDetailRepository.Find(driverID);

                    if (findDriverCreditCardDetail != null)
                    {
                        if (driverCreditCardDetail.cardNo != null)
                        {
                            findDriverCreditCardDetail.cardNo = driverCreditCardDetail.cardNo;
                        }
                        if (driverCreditCardDetail.expiryDate != null)
                        {
                            findDriverCreditCardDetail.expiryDate = driverCreditCardDetail.expiryDate;
                        }
                        if (driverCreditCardDetail.holderName != null)
                        {
                            findDriverCreditCardDetail.holderName = driverCreditCardDetail.holderName;
                        }
                    }
                    unitOfWork.Save();

                }
                catch (Exception e)
                {

                    throw new KeyNotFoundException("CompanyCreditCardDetail is not updated againt given ID", e);
                }
            }
        }
    }
}
