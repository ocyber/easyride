﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.PhoneNumberVerification
{
    public interface IUniqueCodeGenerator
    {
        string generatePhoneNumberVerificationCode();

    }
}
