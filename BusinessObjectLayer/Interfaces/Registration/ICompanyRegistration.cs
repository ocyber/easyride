﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.Registration
{
    interface ICompanyRegistration
    {
        Company RegisterNewCompany(Company company);

        void ActivateCompany(int company);

        void SuspendCompany(int company);

        void updateCredentials(int companyID, CompanyCreditCardDetail creditCardDetail);


    }
}
