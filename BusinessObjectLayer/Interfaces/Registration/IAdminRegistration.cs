﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.Registration
{
    public interface IAdminRegistration
    {

        Admin RegisterNewAdmin(Admin admin, AdminCredential credentials);

        void ActivateAdmin(int admin);

        void SuspendAdmin(int admin);

    }
}
