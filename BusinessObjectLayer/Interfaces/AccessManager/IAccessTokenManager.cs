﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.AccessManager
{
    interface IAccessTokenManager
    {
        string generateNewAccessTokenForDriver(int driverID);
        string generateNewAccessTokenForPassenger(int passengerID);
        string generateNewAccessTokenForAdmin(int adminID);
        bool verifyAdmin(string encryptedToken, out Admin admin);
        bool verifyPassenger(string encryptedToken, out Passenger passenger);
        bool verifyDriver(string encryptedToken, out Driver driver);
        string encodeDriverAccessToken(Driver driver);
        string encodePassengerAccessToken(Passenger passenger);
        string encodeAdminAccessToken(Admin admin);
        void decodeAccessToken(string encodedToken, out int companyID, out string compqnayIdentificationKey, out int driverID, out string driverAccessToken);
       

    }
}
