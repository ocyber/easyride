﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.AdminManager
{
    interface IAdminManager
    {
        void createNewAdmin(Admin admin);
        void activateAdmin(int adminID);
        void deactivateAdmin(int adminID);
        void updateAdmin(int adminID, Admin admin);
        void deleteAdmin(int adminID);
        void updateAdminCredential(int adminID, AdminCredential adminCredential);
    }
}
