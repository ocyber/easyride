﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace BusinessAccessLayer.Interfaces.CompanyManager
{
    interface ICompanyManager
    {
        void createNewCompany(Company company);
        void activateCompany(int companyID);
        void deactivateCompany(int companyID);
        void updateCompany(int companyID, Company company);
        CompanyCreditCardDetail updateCompanyCreditCard(Company company, CompanyCreditCardDetail CreditCardDetail);
        void deleteCompany(int companyID);
    }
}
