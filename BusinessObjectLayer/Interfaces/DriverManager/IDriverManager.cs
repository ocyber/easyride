﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.DriverManager
{
    interface IDriverManager
    {
        void createNewDriver(Driver driver);
        void activateDriver(int driverID);
        void deactivateDriver(int driverID);
        void updateDriver(int driverID, Driver driver);
        void updateDriverCreditCard(int driverID, DriverCreditCardDetail driverCreditCardDetail);
        void deleteDriver(int driverID);
        void updateDriverCredential(int driverID, DriverCredential driverCredential);
    }
}
