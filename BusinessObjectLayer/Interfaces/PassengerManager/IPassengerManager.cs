﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAccessLayer.Interfaces.PassengerManager
{
    interface IPassengerManager
    {
        void createNewPassenger(Passenger passenger);
        void activatePassenger(int passengerID);
        void deactivatePassenger(int passengerID);
        void updatePassenger(int passengerID, Passenger passenger);
        void updatePassengerCreditCard(int passengerID, PassengerCreditCardDetail passengerCreditCardDetail);
        void deletePassenger(int passengerID);
        void updatePassengerCredential(int passengerID, PassengerCredential passengerCredential);
    }
}
