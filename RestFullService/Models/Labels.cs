﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestFullService.Models
{
    public static class Labels
    {
        public static string USERNAME = "username";
        public static string PASSWORD = "password";
        public static string EMAIL = "email";
        public static string AUTHTOKEN = "authtoken";
        public static string ACCESSLEVEL = "accesslevel";
        public static string FIRSTNAME = "firstname";
        public static string LASTNAME = "lastname";
        public static string MESSAGE = "message";

        public static string COMPENSATIONSTRATEGY = "compensation";

        public static string CREDITCARD = "creditcard";
    }
}