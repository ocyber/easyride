﻿using ExpressiveAnnotations.Attributes;
using RestFullService.Filters;
using RestFullService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestFullService.Controllers
{
    [RoutePrefix("Admin")]
    public class AdminLoginController : BaseWebApiController
    {
        [ValidateModel,HttpPost,Route("login"),AutoResponce]
        public void LoginByUserNamePassword([FromBody]LoginRequest req)
        {
            string accessTokenIfVerified=null;
            if (req.UserName!=null)
            {
                 accessTokenIfVerified= unitOfBussinessLogic.AdminManager.LoginByUserName(req.UserName, req.Password, out callerAdmin);


                if (accessTokenIfVerified==null)
                {
                    accessTokenIfVerified = unitOfBussinessLogic.AdminManager.LoginByEmailId(req.UserName, req.Password, out callerAdmin);
                }
            }


            if (accessTokenIfVerified != null)
            {
                ResponceBuilder.Add(Labels.FIRSTNAME, callerAdmin.firstName);
                ResponceBuilder.Add(Labels.LASTNAME, callerAdmin.lastName);
                ResponceBuilder.Add(Labels.AUTHTOKEN, accessTokenIfVerified);
               
            }
            else
            {
                ResponceBuilder.Add(Labels.MESSAGE, "Login Failed ! Username o password is invalid");
                httpStatusCode = HttpStatusCode.Unauthorized;
            }

        }

        [HttpPost,Route("verify")]
        public object verifyAdminTest([FromBody]VerificationRequest token)
        {
           return unitOfBussinessLogic.AccessManager.verifyAdmin(token.token,out callerAdmin);
        }

        public class VerificationRequest
        {
            public string token { get; set; }
        }
        public class LoginRequest
        {

            [Required]
            public string UserName { get; set; }

           
            [Required]
            public string Password { get; set; }
        }

    }
}
