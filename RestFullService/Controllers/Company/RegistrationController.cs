﻿using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;

using RestFullService.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestFullService.Controllers.CompanyControllers
{
    [RoutePrefix("Company")]
    public class CompanyController : BaseWebApiController
    {
       
        [HttpPost,Route("Register"),ValidateModel]
        public object RegisterCompany([FromBody]CompanyRegistrationRequest req)
        {
            Company company=new Company();
            company.name = req.Company.Name;
            company.address = req.Company.Address;
            company.email = req.Company.Email;
            company.phoneNo = req.Company.PhoneNumber;

            if (req.Company.Mode==0)
            {
                company.setPublic(false);
            }
            else
            {
                company.setPublic(false);
            }
            company.setActive(false);

            company.setTypeClient(true);

            unitOfBussinessLogic.CompanyManager.createNewCompany(company);
            
            Admin admin = new Admin();


            admin.firstName = req.Admin.FirstName;
            admin.lastName = req.Admin.LastName;
            admin.phoneno = req.Admin.LastName;
            admin.setActive(true);
            admin.companyID = company.id;


            unitOfBussinessLogic.AdminManager.createNewAdmin(admin);


            AdminCredential adminCredential = new AdminCredential();

            adminCredential.username = req.Admin.UserName;
            adminCredential.password = req.Admin.Password;

            unitOfBussinessLogic.unitOfWork.Save();

            unitOfBussinessLogic.AdminManager.AssignCredentials(admin.id, adminCredential);



            return req;


        }








        public class CompanyRegistrationRequest
        {
            [Required]
            public CompanyDetail Company { get; set; }
            [Required]
            public AdminDetail Admin { get; set; }

        }
        
        public class CompanyDetail
        {
            [Required]
            public string Name { get; set; }
            [Required]
            public string Address { get; set; }
            [Required]
            public string City { get; set; }
            [Required]
            public string PhoneNumber { get; set; }
            [Required]
            public string Email { get; set; }
            [Required]
            public int Mode { get; set; }
        }
        public class AdminDetail
        {
            [Required]
            public string FirstName { get; set; }
            [Required]
            public string LastName { get; set; }
            [Required]
            public string PhoneNumber { get; set; }
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
        }
    }
}
