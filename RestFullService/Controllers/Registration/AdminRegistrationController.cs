﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestFullService.Controllers.Registration
{
    public class AdminRegistrationController : BaseWebApiController
    {
        
        public HttpResponseMessage CreateNewAdmin([FromBody] RegistrationRequest req)
        {

            return null;
        }

        

        public HttpResponseMessage CreateFirstAdminOfNewCompany([FromBody] NewCompanyAdminRegistrationRequest req)
        {
           



            return null;
        }





        public class RegistrationRequest
        {

            [Required]
            [StringLength(22)]
            public string FirstName { get; set; }

            [Required]
            [StringLength(22)]
            public string LastName { get; set; }

            [Required]
            public string PhoneNo { get; set; }
            [Required]
            public string Email { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public int AccessLevel { get; set; }
            

        }

        public class NewCompanyAdminRegistrationRequest :RegistrationRequest
        {
            [Required]
            public int CompanyID { get; set; }
        }




    }
}
