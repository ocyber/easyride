﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessAccessLayer.UnitOfBusinessLogicNameSpace;
using BusinessAccessLayer.BussinessLayer;
using RestFullService.Filters;
using System.ComponentModel.DataAnnotations;

namespace RestFullService.Controllers.PhoneNumberVerification
{
    public partial class PhoneNumberVerificationController : BaseWebApiController
    {

        [HttpPost, ValidateModel, Route("PhoneNumberVerificationRequest"),AutoResponce]
        public void VerificationRequests([FromBody]VerificationRequest Req)
        {

            //creating phone verification request record in database
            int secondRemainingToTimeoutCode = 0;
            PhoneNumberVerificationLayer BAL = unitOfBussinessLogic.PhoneNumberVerification;
           int key= BAL.CreateVerificationRequest(Req.PhoneNumber, out secondRemainingToTimeoutCode);

            string encryptKey = unitOfBussinessLogic.UniqueCodeGenerator.RandomString(3, true, false);
            ResponceBuilder.Add("Key",encryptKey+ unitOfBussinessLogic.CryptoGraphy.Encrypt(key.ToString(),encryptKey ));
            

            ResponceBuilder.Add("Timeout", secondRemainingToTimeoutCode);
            ResponceBuilder.Add("PhoneNumber", Req.PhoneNumber);
            httpStatusCode = HttpStatusCode.OK;

            
        }

        public class VerificationRequest
        {
            [Required]
            public string PhoneNumber { get; set; }
        }
    }
}
