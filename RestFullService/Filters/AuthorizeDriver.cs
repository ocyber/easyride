﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;

namespace RestFullService.Filters
{
    public class AuthorizeDriver : LoadDependenciesFromControllerContextFilter
    {


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            string Token = "";
            base.OnActionExecuting(actionContext);
            if (unitOfBusinessLogic.AccessManager.verifyDriver(Token, out callerDriver))
            {
                callerCompany = callerAdmin.Company;
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
             HttpStatusCode.Unauthorized, "Authorization Failed! HTTP Request Header Must Contain A Valid AuthToken for authorization");
            }

            controller.callerDriver = callerDriver;
            controller.callerCompany = callerCompany;

        }
    }


}