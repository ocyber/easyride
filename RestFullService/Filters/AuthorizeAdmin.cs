﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;

namespace RestFullService.Filters
{
    public class AuthorizeAdmin : LoadDependenciesFromControllerContextFilter
    {
        public int AccessLevel { get; set; }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string Token = "";
            base.OnActionExecuting(actionContext);
            
                if (unitOfBusinessLogic.AccessManager.verifyAdmin(Token, out callerAdmin) && callerAdmin.AdminCredential.accessLevel >= AccessLevel)
                {
                    callerCompany = callerAdmin.Company;
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                 HttpStatusCode.Unauthorized, "Authorization Failed! HTTP Request Header Must Contain A Valid AuthToken for authorization");
                }
            

            controller.callerAdmin = callerAdmin;
            controller.callerCompany = callerCompany;

        }
    }
}