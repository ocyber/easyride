﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace RestFullService.Filters
{
    public class AutoResponce :LoadDependenciesFromControllerContextFilter
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {

            try
            {
                if (ResponceBuilder != null )
                {
                    actionExecutedContext.ActionContext.Response = actionExecutedContext.ActionContext.Request.CreateResponse(
                 controller.httpStatusCode, ResponceBuilder);
                }
            }
            catch (Exception) { }
            
        }
    }
}