﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories
{
    public class SharedRideRepository : GenericRepository<SharedRide>, ISharedRideRepository
    {
        public SharedRideRepository(DatabaseContext context) : base(context)
        {
        }

        public Entities.Passenger getConsumerPassenegr(int id)
        {
            try
            {
                return DbSet.Find(id).Passenger1;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("SharedRide is not found against given ID", e);
            }
        }

        public Entities.Passenger getDonorPassenger(int id)
        {
            try
            {
                return DbSet.Find(id).Passenger;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("SharedRide is not found against given ID", e);
            }
        }

        public PromotionCode getPromotionCode(int id)
        {
            try
            {
                return DbSet.Find(id).PromotionCode;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("SharedRide is not found against given ID", e);
            }
        }

        public RideActivity getRideActivity(int id)
        {
            try
            {
                return DbSet.Find(id).RideActivity;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("SharedRide is not found against given ID", e);
            }
        }
    }
}
