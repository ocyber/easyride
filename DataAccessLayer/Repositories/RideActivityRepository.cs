﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories
{
    public class RideActivityRepository : GenericRepository<RideActivity>, IRideActivityRepository
    {
        public RideActivityRepository(DatabaseContext context) : base(context)
        {
        }

        public Driver getDriver(int id)
        {
            try
            {
                return DbSet.Find(id).Driver;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("RideActivity not found against Provided ID", e);
            }

        }

        public Entities.Passenger getPassenger(int id)
        {
            try
            {
                return DbSet.Find(id).Passenger;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("RideActivity not found against Provided ID", e);
            }
        }

        public RideBillingTransaction getRideBillingTransaction(int id)
        {
            try
            {
                return DbSet.Find(id).RideBillingTransaction;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("RideActivity not found against Provided ID", e);
            }
        }

        public RideInfo getRideInfo(int id)
        {
            try
            {
                return DbSet.Find(id).RideInfo;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("RideActivity not found against Provided ID", e);
            }
        }

        public RideRateStrategy getRideRateStrategy(int id)
        {
            try
            {
                return DbSet.Find(id).RideRateStrategy;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("RideActivity not found against Provided ID", e);
            }
        }

        
    }
}
