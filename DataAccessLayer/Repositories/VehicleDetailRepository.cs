﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories
{
    public class VehicleDetailRepository : GenericRepository<VehicleDetail>, IVehicleDetailRepository
    {
        public VehicleDetailRepository(DatabaseContext context) : base(context)
        {
        }

        public Driver getDriver(int id)
        {
            try
            {
                return DbSet.Find(id).Driver;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("VehicleDetail is not found against given ID",e);
            }
        }

        public VehicleCategory getVehicalCatagory(int id)
        {
            try
            {
                return DbSet.Find(id).VehicleCategory;
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("VehicleDetail is not found against given ID", e);
            }
        }
    }
}
