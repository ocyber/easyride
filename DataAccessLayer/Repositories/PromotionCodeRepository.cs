﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories
{
    public class PromotionCodeRepository : GenericRepository<PromotionCode>, IPromotionCodeRepository
    {
        public PromotionCodeRepository(DatabaseContext context) : base(context)
        {
        }

        public IQueryable<GeneralPromotion> getGeneralPromotion(int id)
        {
            try
            {
                return DbSet.Find(id).GeneralPromotions.AsQueryable();
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("PromotionCode is not found against given ID", e);
            }
        }

        public IQueryable<RideBillingTransaction> getRideBillingTransaction(int id)
        {
            try
            {
                return DbSet.Find(id).RideBillingTransactions.AsQueryable<RideBillingTransaction>();
            }
            catch (Exception e)
            {

                throw new KeyNotFoundException("PromotionCode is not found against given ID", e);
            }
        }

        public SharedRide getSharedRide(int id)
        {
            try
            {
                return DbSet.Find(id).SharedRides.AsQueryable<SharedRide>();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
