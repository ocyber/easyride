﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{

    public interface IGenericRepository<TEntity> where TEntity : class
    {




        IQueryable<TEntity> getAll();
        
        TEntity AddNew(TEntity entity);
        
        TEntity Find(int ID);
        
        
        void Delete(int id);
        void Save();

        void SaveAsync();


    }
}
