﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IPassengerRepository
    {
        PassengerCreditCardDetail getPassengerCreditCardDetail(int id);
        PassengerConnectivityDetail getPassengerConnectivityDetail(int id);
        PassengerCredential getPassengerCredential(int id);
        IQueryable<RideActivity> getRideActivities(int id);
        //IQueryable<RideBillingTransaction> getRideBillingTransactions(int id);

        //IQueryable<SharedRide> getDonatedSharedRides(int id);

        //IQueryable<SharedRide> getConsumedSharedRide(int id);

        Company getCompany(int id);

    }
}
