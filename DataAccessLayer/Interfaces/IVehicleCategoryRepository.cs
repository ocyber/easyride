﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IVehicleCategoryRepository
    {
        Company getCompany(int id);
        IQueryable<RideRateStrategy> getRideRateStrategy(int id);
        IQueryable<VehicleDetail> getVehicalDetail(int id);

        
    }
}
