﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
   public interface ICredentialDetailRepository
    {
        void allocateNewAccessToken(int id);
        string status(int id);
        void updateStatus(string status, int id);
    }
}
