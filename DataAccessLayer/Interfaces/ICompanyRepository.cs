﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ICompanyRepository
    {
        void renewIdentificationKey(int id);

        

        string getStatus(int id);

        IQueryable<Admin> getAdmins(int id);

        CompanyCreditCardDetail getCreditCardDetail(int id);

        IQueryable<DriverCompensationStrategy> getDriverCompensationStrategies(int id);

        //IQueryable<RideBillingTransaction> getRideBillingTansaction(int id);

        //IQueryable<RideRateStrategy> getRideRateStrategy(int id);

        IQueryable<VehicleCategory> getVehicalDetails(int id);

       

    }
}
