﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IAdminRepository
    {
        Company getCompany(int id);
        AdminConnectivityDetail getAdminConnectivityDetail(int id);
        AdminCredential getAdminCredential(int id);
    }
}
