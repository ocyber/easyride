﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
   public interface IConnectivityDetailRepository
    {
        void updateFCMToken(string token, int id);

        void setSignalRConnectionID(string connectionID, int id);
        bool isFCMEnabled(int id);

        
        bool isSignalRConnected(int id);


        void setSignalRConnected(int id,bool flag);

    }
}
