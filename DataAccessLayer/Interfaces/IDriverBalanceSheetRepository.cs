﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
   public interface IDriverBalanceSheetRepository
    {

        double getBalance(int id);

        double addBalance(double balance, int id);

        double deductBalance(double balance, int id);

        Driver getDriver(int id);

    }
}
