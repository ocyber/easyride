﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IDriverRepository
    {
        DriverCreditCardDetail getDriverCreditCardDetail(int id);
        DriverBalanceSheet getDriverBalanceSheet(int id);
        DriverConnectivityDetail getDriverConnectivityDetail(int id);
        DriverCredential getDriverCredential(int id);
        IQueryable<RideActivity> getRideActivities(int id);
        //IQueryable<RideBillingTransaction> getRideBillingTransactions(int id);
        VehicleDetail getVehicleDetail(int id);


    }
}
