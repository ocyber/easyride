﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IRideBillingTransactionRepositry
    {
        //Company getCompany(int id);
        //Driver getDriver(int id);
        //Passenger getPassenger(int id);
        //PromotionCode getPromotionCode(int id);
        RideActivity getRideActivity(int id);

    }
}
