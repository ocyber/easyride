﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IPromotionCodeRepository
    {
        IQueryable<GeneralPromotion> getGeneralPromotion(int id);
        IQueryable<RideBillingTransaction> getRideBillingTransaction(int id);
        SharedRide getSharedRide(int id);
    }
}
