﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IRideActivityRepository
    {
        Driver getDriver(int id);
        Passenger getPassenger(int id);
        RideRateStrategy getRideRateStrategy(int id);
        RideBillingTransaction getRideBillingTransaction(int id);
        RideInfo getRideInfo(int id);
       
    }
}
