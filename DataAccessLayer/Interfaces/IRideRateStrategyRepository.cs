﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface IRideRateStrategyRepository
    {
        //Company getCompany(int id);
        IQueryable<RideActivity> getRideActivities(int id);
        VehicleCategory getVehicleCategory(int id);
    }
}
