﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    interface ISharedRideRepository
    {
        Passenger getDonorPassenger(int id);
        Passenger getConsumerPassenegr(int id);
        PromotionCode getPromotionCode(int id);
        RideActivity getRideActivity(int id);
    }
}
