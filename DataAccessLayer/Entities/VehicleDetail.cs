namespace DataAccessLayer.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleDetail")]
    public partial class VehicleDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string VIN { get; set; }

        [StringLength(20)]
        public string model { get; set; }

        [Required]
        [StringLength(20)]
        public string color { get; set; }

        public int? conditionScore { get; set; }

        public int vehicleCategoryID { get; set; }

        public DbGeometry lastKnownLocation { get; set; }

        public DateTime? timeStamp { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual VehicleCategory VehicleCategory { get; set; }
    }
}
