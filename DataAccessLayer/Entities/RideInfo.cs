namespace DataAccessLayer.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RideInfo")]
    public partial class RideInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RideInfo()
        {
            RideActivities = new HashSet<RideActivity>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public DbGeometry pickupLocation { get; set; }

        public DbGeometry destinationLocation { get; set; }

        public string pickupAddress { get; set; }

        public string destinationAddress { get; set; }

        public DbGeometry route { get; set; }

        public string extraDetail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RideActivity> RideActivities { get; set; }

        public virtual RideActivity RideActivity { get; set; }
    }
}
