namespace DataAccessLayer.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Admin")]
    public partial class Admin
    {
        public int id { get; set; }

        [StringLength(22)]
        public string firstName { get; set; }

        [StringLength(22)]
        public string lastName { get; set; }

        [StringLength(13)]
        public string phoneno { get; set; }

        public int? companyID { get; set; }

        [StringLength(20)]
        public string status { get; set; }

        [StringLength(10)]
        public string referenceAdminID { get; set; }

        public virtual Company Company { get; set; }

        public virtual AdminConnectivityDetail AdminConnectivityDetail { get; set; }

        public virtual AdminCredential AdminCredential { get; set; }
    }
}
