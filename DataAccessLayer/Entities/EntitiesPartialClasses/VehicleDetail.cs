﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public partial class VehicleDetail
    {
        public bool isCurrentlyAvailable()
        {
            if (timeStamp>=(DateTime.Now-TimeSpan.FromSeconds(15)) && Driver.availabilityStatus== Flags.ACCEPTING)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void setCurrentLocation(DbGeometry location)
        {
            if (location!=null)
            {

                lastKnownLocation = location;
                timeStamp = DateTime.Now;
            }
        }




    }
}
