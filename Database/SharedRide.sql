﻿CREATE TABLE [dbo].[SharedRide]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [consumerPassengerID] INT NOT NULL, 
    [donerPassengerID] INT NOT NULL, 
    [promotionCodeID] INT NOT NULL, 
    [rideActivityID] INT NOT NULL, 
    CONSTRAINT [FK_SharedRide_ToRideActivity] FOREIGN KEY ([rideActivityID]) REFERENCES [RideActivity]([id]), 
    CONSTRAINT [FK_SharedRide_ToPromotionCode] FOREIGN KEY ([promotionCodeID]) REFERENCES [PromotionCode]([id]), 
    CONSTRAINT [FK_SharedRide_ToDonerPassenger] FOREIGN KEY ([donerPassengerID]) REFERENCES [Passenger]([id]),
	CONSTRAINT [FK_SharedRide_ToConsumerPassenger] FOREIGN KEY ([consumerPassengerID]) REFERENCES [Passenger]([id])
)
