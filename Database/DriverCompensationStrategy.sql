﻿CREATE TABLE [dbo].[DriverCompensationStrategy]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [driverPercentage] FLOAT NOT NULL DEFAULT 80, 
    [validFrom] DATETIME NOT NULL, 
    [validTo] DATETIME NULL, 
    [companyID] INT NOT NULL, 
    CONSTRAINT [FK_DriverCompensationStrategy_ToCompany] FOREIGN KEY ([companyID]) REFERENCES [Company]([id]) ON DELETE CASCADE 
)
