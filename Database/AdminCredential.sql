﻿CREATE TABLE [dbo].[AdminCredential]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [username] VARCHAR(20) NULL, 
    [password] VARCHAR(20) NULL, 
    [accessToken] VARCHAR(50) NULL, 
    [accessTokenExpiry] DATETIME NULL, 
    [status] VARCHAR(20) NULL, 
    [accessLevel] INT NULL, 
    [email] VARCHAR(50) NULL, 
    CONSTRAINT [FK_AdminCredential_ToAdmin] FOREIGN KEY ([id]) REFERENCES [Admin]([id]) ON DELETE CASCADE
)
