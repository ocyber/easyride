﻿CREATE TABLE [dbo].[RideActivity]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [passengerID] INT NOT NULL, 
    [driverID] INT NULL, 
    [requestStatus] VARCHAR(20) NOT NULL, 
    [requestTime] DATETIME NOT NULL, 
    [activityStatus] VARCHAR(50) NULL, 
    [rideStartTime] DATETIME NULL, 
    [dropOffTime] DATETIME NULL, 
    [pickupArrivalTime] DATETIME NULL, 
    [distanceTraveled] FLOAT NULL, 
    [duration] INT NULL, 
    [rideInfoID] INT NULL, 
    [rideRateStrategyID] INT NULL, 
    [surge] FLOAT NULL DEFAULT 1, 
    [paymentMode] VARCHAR(20) NULL, 
    CONSTRAINT [FK_RideActivity_ToPassenger] FOREIGN KEY ([passengerID]) REFERENCES [Passenger]([id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_RideActivity_ToDriver] FOREIGN KEY ([driverID]) REFERENCES [Driver]([id]), 
    CONSTRAINT [FK_RideActivity_ToRate] FOREIGN KEY ([rideRateStrategyID]) REFERENCES [RideRateStrategy]([id]), 
    CONSTRAINT [FK_RideActivity_ToRideInfo] FOREIGN KEY ([rideInfoID]) REFERENCES [RideInfo]([id])
)
