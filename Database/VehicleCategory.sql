﻿CREATE TABLE [dbo].[VehicleCategory]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [name] VARCHAR(30) NOT NULL, 
    [capacity] INT NOT NULL, 
    [description] VARCHAR(MAX) NULL, 
    [companyID] INT NOT NULL, 
    CONSTRAINT [FK_VehicleCategory_ToCompany] FOREIGN KEY ([companyID]) REFERENCES [Company]([id])
)
