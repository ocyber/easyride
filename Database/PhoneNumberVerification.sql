﻿CREATE TABLE [dbo].[PhoneNumberVerification]
(
	[Id] INT NOT NULL PRIMARY KEY identity(1,1),
	[phoneNumber] varchar(15) not null,
	[verificationCode] varchar(5) not null,
	[expiryDate] datetime not null


)
