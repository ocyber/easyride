﻿CREATE TABLE [dbo].[Passenger]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [firstName] VARCHAR(22) NOT NULL, 
    [lastName] VARCHAR(22) NOT NULL, 
    [phoneNo] VARCHAR(13) NOT NULL, 
    [companyID] INT NOT NULL, 
    [defaultPaymentMode] VARCHAR(20) NOT NULL , 
    [status] NCHAR(20) NOT NULL , 
    CONSTRAINT [FK_Passenger_ToCompany] FOREIGN KEY ([companyID]) REFERENCES [Company]([id]) 
)
