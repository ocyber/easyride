﻿CREATE TABLE [dbo].[Promotion]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [promotionType] VARCHAR(20) NOT NULL, 
    [discountPercentage] INT NULL, 
    [discountDedecatedAmount] INT NULL, 
    [maxDiscount] INT NOT NULL, 
    [description] VARCHAR(50) NULL, 
    [promotionCode] VARCHAR(20) NULL, 
    [expiry] DATETIME NULL 
)
