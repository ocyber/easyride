﻿CREATE TABLE [dbo].[PassengerCredential]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [username] VARCHAR(20) NULL, 
    [password] VARCHAR(20) NULL, 
    [accessToken] VARCHAR(50) NOT NULL, 
    [accessTokenExpiry] DATETIME NULL, 
    [status] VARCHAR(20) NOT NULL, 
    [email] VARCHAR(50) NULL, 
    CONSTRAINT [FK_PassengerCredential_To[Passenger] FOREIGN KEY ([id]) REFERENCES [Passenger]([id]) ON DELETE CASCADE
)
