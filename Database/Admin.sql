﻿CREATE TABLE [dbo].[Admin]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [firstName] VARCHAR(22) NULL, 
    [lastName] VARCHAR(22) NULL, 
    [phoneno] VARCHAR(13) NULL, 
    [companyID] INT NULL, 
    [status] VARCHAR(20) NULL , 
    [referenceAdminID] NCHAR(10) NULL, 
    CONSTRAINT [FK_Admin_ToCompany] FOREIGN KEY ([companyID]) REFERENCES [Company]([id]) ON DELETE CASCADE 
)
