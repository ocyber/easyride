﻿CREATE TABLE [dbo].[VehicleDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [VIN] VARCHAR(20) NOT NULL, 
    [model] VARCHAR(20) NULL, 
    [color] VARCHAR(20) NOT NULL, 
    [conditionScore] INT NULL, 
    [vehicleCategoryID] INT NOT NULL, 
    [lastKnownLocation] [sys].[geometry] NULL, 
    [timeStamp] DATETIME NULL, 
    CONSTRAINT [FK_VechicleDetail_ToDriver] FOREIGN KEY ([id]) REFERENCES [Driver]([id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_VechicleDetail_ToVehicleCategory] FOREIGN KEY ([vehicleCategoryID]) REFERENCES [VehicleCategory]([id])
)
