﻿CREATE TABLE [dbo].[RideBillingTransaction]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [passengerPayable] INT NOT NULL, 
    [companyServiceCharges] FLOAT NOT NULL, 
    [overallStatus] VARCHAR(20) NOT NULL, 
    [driverID] INT NOT NULL, 
    [companyID] INT NOT NULL, 
    [passengerID] INT NOT NULL, 
    [driverPayable] FLOAT NULL, 
    [driverPayableStatus] VARCHAR(20) NULL, 
    [paymentMode] VARCHAR(20) NULL, 
    [passengerPayableStatus] VARCHAR(20) NULL, 
    [companyPayable] FLOAT NULL, 
    [companyPayableStatus] VARCHAR(20) NULL, 
    [promotionID] INT NULL, 
    CONSTRAINT [FK_RideBillingTransaction_ToRideActivity] FOREIGN KEY ([id]) REFERENCES [RideActivity]([id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_RideBillingTransaction_ToPromotion] FOREIGN KEY ([promotionID]) REFERENCES [Promotion]([id]) 
    
)
