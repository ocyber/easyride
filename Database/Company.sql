﻿CREATE TABLE [dbo].[Company]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [name] VARCHAR(MAX) NULL, 
    [address] VARCHAR(MAX) NULL, 
    [phoneNo] VARCHAR(13) NULL, 
    [identificationKey] VARCHAR(50) NULL, 
    [status] VARCHAR(20) NULL , 
    [email] VARCHAR(50) NULL, 
    [mode] VARCHAR(20) NULL DEFAULT 'public', 
    [type] VARCHAR(20) NULL 
)
