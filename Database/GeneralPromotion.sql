﻿CREATE TABLE [dbo].[GeneralPromotion]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [promotionType] VARCHAR(20) NOT NULL, 
    [discountPercentage] INT NULL, 
    [discountDedecatedAmount] INT NULL, 
    [maxDiscount] INT NOT NULL, 
    [description] VARCHAR(50) NULL, 
    [promotionCodeID] INT NOT NULL, 
    CONSTRAINT [FK_GeneralPromotion_ToPermotionCode] FOREIGN KEY ([promotionCodeID]) REFERENCES [PromotionCode]([id])
)
