﻿CREATE TABLE [dbo].[PassengerCreditCardDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [holderName] VARCHAR(30) NOT NULL, 
    [cardNo] VARCHAR(20) NOT NULL, 
    [ccvNo] VARCHAR(3) NOT NULL, 
    [expiryDate] DATETIME NOT NULL, 
    CONSTRAINT [FK_CompanyCreditCardDetail_ToPassenger] FOREIGN KEY ([id]) REFERENCES [Passenger]([id]) ON DELETE CASCADE 
)
