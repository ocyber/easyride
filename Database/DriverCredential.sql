﻿CREATE TABLE [dbo].[DriverCredential]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [username] VARCHAR(20) NULL, 
    [password] VARCHAR(20) NULL, 
    [accessToken] VARCHAR(50) NOT NULL, 
    [accessTokenExpiry] DATETIME NULL, 
    [status] VARCHAR(20) NOT NULL, 
    [email] VARCHAR(50) NULL, 
    CONSTRAINT [FK_DriverCredential_ToDriver] FOREIGN KEY ([id]) REFERENCES [Driver]([id]) ON DELETE CASCADE
)
