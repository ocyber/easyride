﻿CREATE TABLE [dbo].[PassengerConnectivityDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [fcmToken] VARCHAR(MAX) NULL, 
    [signalRConnectionID] VARCHAR(MAX) NULL, 
    [isConnected] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_PassengerConnectivityDetail_ToPassenger] FOREIGN KEY ([id]) REFERENCES [Passenger]([id])
)


GO

CREATE INDEX [IX_PassengerConnectivityDetail_isConnected] ON [dbo].[PassengerConnectivityDetail] ([isConnected])
