﻿CREATE TABLE [dbo].[AdminConnectivityDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [fcmToken] VARCHAR(MAX) NULL, 
    [signalRConnectivityID] VARCHAR(MAX) NULL, 
    [isConnected] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_AdminConnectivityDetail_ToAdmin] FOREIGN KEY ([id]) REFERENCES [Admin]([id])
)

GO
CREATE INDEX [IX_AdminConnectivityDetail_isConnected] ON [dbo].[AdminConnectivityDetail] ([isConnected])
