﻿CREATE TABLE [dbo].[DriverConnectivityDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [fcmToken] VARCHAR(MAX) NULL, 
    [SignalRConnectivityID] VARCHAR(MAX) NULL, 
    [isConnected] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_DriverConnectivityDetail_ToDriver] FOREIGN KEY ([id]) REFERENCES [Driver]([id])
)


GO

CREATE INDEX [IX_DriverConnectivityDetail_isConnected] ON [dbo].[DriverConnectivityDetail] ([isConnected])
