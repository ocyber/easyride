﻿CREATE TABLE [dbo].[CompanyCreditCardDetail]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [holderName] VARCHAR(30) NOT NULL, 
    [cardNo] VARCHAR(20) NOT NULL, 
    [ccvNo] VARCHAR(3) NOT NULL, 
    [expiryDate] DATETIME NOT NULL, 
    CONSTRAINT [FK_CompanyCreditCardDetail_ToCompany] FOREIGN KEY ([id]) REFERENCES [Company]([id]) ON DELETE CASCADE 
)
