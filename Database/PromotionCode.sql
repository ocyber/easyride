﻿CREATE TABLE [dbo].[PromotionCode]
(
	[id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [code] VARCHAR(20) NOT NULL, 
    [expiry] DATETIME NOT NULL, 
    [type] VARCHAR(20) NOT NULL
)
