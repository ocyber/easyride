﻿CREATE TABLE [dbo].[RideRateStrategy]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [perKmMoving] FLOAT NOT NULL, 
    [perMinuteMoving] FLOAT NOT NULL, 
    [strategyType] VARCHAR(20) NOT NULL, 
    [validFrom] DATETIME NOT NULL, 
    [validTo] DATETIME NULL, 
    [perMinuteWaiting] FLOAT NOT NULL, 
    [perMinuteChargesTimeThreshold] INT NULL, 
    [perMinuteChargesSpeedthreshold] INT NULL, 
    [vehicleCatagoryID] INT NOT NULL, 
    CONSTRAINT [FK_RideRateStrategy_ToVehicalCatagory] FOREIGN KEY ([vehicleCatagoryID]) REFERENCES [VehicleCategory]([id])
)
