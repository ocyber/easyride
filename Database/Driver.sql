﻿CREATE TABLE [dbo].[Driver]
(
	[id] INT NOT NULL PRIMARY KEY identity (1,1), 
    [firstName] VARCHAR(22) NOT NULL, 
    [lastName] VARCHAR(22) NOT NULL, 
    [phoneNo] VARCHAR(13) NOT NULL, 
    [cnic] VARCHAR(13) NOT NULL, 
    [DOB] DATE NOT NULL, 
    [photo] VARCHAR(50) NULL, 
    [companyID] INT NOT NULL, 
    [status] VARCHAR(20) NOT NULL, 
    [availabilityStatus] VARCHAR(20) NOT NULL, 
    [referenceAdminID] NCHAR(10) NULL, 
    CONSTRAINT [FK_Driver_ToCompany] FOREIGN KEY ([companyID]) REFERENCES [Company]([id]) 
)


