﻿CREATE TABLE [dbo].[RideInfo]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [pickupLocation] [sys].[geometry] NULL, 
    [destinationLocation] [sys].[geometry] NULL, 
    [pickupAddress] VARCHAR(MAX) NULL, 
    [destinationAddress] VARCHAR(MAX) NULL, 
    [route] [sys].[geometry] NULL, 
    [extraDetail] VARCHAR(MAX) NULL, 
    CONSTRAINT [FK_RideInfo_ToRideActivity] FOREIGN KEY ([id]) REFERENCES [RideActivity]([id]) ON DELETE CASCADE
)
