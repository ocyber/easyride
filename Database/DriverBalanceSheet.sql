﻿CREATE TABLE [dbo].[DriverBalanceSheet]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [balance] FLOAT NOT NULL DEFAULT 0.0, 
    CONSTRAINT [FK_DriverBalanceSheet_ToDriver] FOREIGN KEY ([id]) REFERENCES [Driver]([id]) ON DELETE CASCADE
)
